#include "Messages.h"

Messages::Messages()
{
	this->_sender = "";
	this->_reciver = "";
	this->_data = "";
}

Messages::Messages(string sender, string reciver, string data)
{
	this->_sender = sender;
	this->_reciver = reciver;
	this->_data = data;
}

string Messages::get_sender()
{
	return this->_sender;
}

string Messages::get_reciver()
{
	return this->_reciver;
}

string Messages::get_data()
{
	return this->_data;
}

string Messages::get_filename(string sender, string reciver)
{
	return (sender > reciver ? reciver + '&' + sender : sender + '&' + reciver) + ".txt";
}
