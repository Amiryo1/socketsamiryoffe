#pragma once
#include <string>

using std::string;

class Messages
{
private:
	string _sender;
	string _reciver;
	string _data;
public:
	
	Messages();
	Messages(string sender, string reciver, string data);
	~Messages() { };
	

	string get_sender();
	string get_reciver();
	string get_data();
	static string get_filename(string sender, string reciver);
};

