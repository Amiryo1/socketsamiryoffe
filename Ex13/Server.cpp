#include "Server.h"


Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	std::cout << "binded" << port << std::endl;

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "listening..." << std::endl;
}


SOCKET Server::acceptClient()
{
	// this accepts the client and create a specific socket from server to this client
	// the process will not continue until a client connects to the server
	SOCKET client_socket = accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted !" << std::endl;
	// the function that handle the conversation with the client

	return client_socket;
}


void Server::clientHandler(SOCKET clientSocket)
{
	int reciver_username_len = 0, data_len = 0;
	string sender_username, sender_user_id, reciver_username, data, file_content;
	ifstream log_file;
	Messages message;

	try
	{
		sender_user_id = std::to_string(clientSocket);

		sender_username = login_user(clientSocket);

		while (true)
		{
			Helper::getMessageTypeCode(clientSocket); // we don't need the code but need to remove it from message

			reciver_username_len = Helper::getIntPartFromSocket(clientSocket, 2);

			if (reciver_username_len)
			{
				reciver_username = Helper::getStringPartFromSocket(clientSocket, reciver_username_len);
				data_len = Helper::getIntPartFromSocket(clientSocket, 5);

				if (data_len)
				{
					data = Helper::getStringPartFromSocket(clientSocket, data_len);

					message = Messages(sender_username, reciver_username, data);

					std::unique_lock<mutex> q_locker(this->_messages_mu);

					this->_messages.push(message);

					q_locker.unlock();

					this->_messages_cond.notify_one();
				}

				Helper::send_update_message_to_client(clientSocket, Helper::readFileToString(Messages::get_filename(sender_username, reciver_username)), reciver_username, this->all_users());

				//cout << sender_username << " updated1" << std::endl;
			}
			else
			{
				//clean buffer
				Helper::getStringPartFromSocket(clientSocket, reciver_username_len);
				Helper::getIntPartFromSocket(clientSocket, 5);
				Helper::getStringPartFromSocket(clientSocket, data_len);

				Helper::send_update_message_to_client(clientSocket, "", "", this->all_users());
			}
		}
	}
	catch (const std::exception& e)
	{
		cout << e.what() << std::endl;
		closesocket(clientSocket);
		logout_user(sender_username, sender_user_id);
	}
}

void Server::accecpt_clients()
{
	while (true)
	{
		SOCKET client_socket;
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		cout << "accepting client..." << std::endl;
		
		client_socket = acceptClient();

		auto t = std::thread(&Server::clientHandler, this, client_socket);
		t.detach();
	}
}

string Server::login_user(SOCKET clientSocket)
{
	string username;
	
	try
	{
		Helper::getMessageTypeCode(clientSocket); // we don't need the code but need to remove it from message

		int len = Helper::getIntPartFromSocket(clientSocket, 2); // get length of name, max two digits

		username = Helper::getStringPartFromSocket(clientSocket, len);

		std::unique_lock<mutex> users_locker(this->_users_mu);

		this->_users.push_back(username);

		users_locker.unlock();

		Helper::send_update_message_to_client(clientSocket, "", "", this->all_users());

		cout << "ADDED new client " << std::to_string(clientSocket) << ", " << username << " to clients list" << std::endl;
	}
	catch (const std::exception& e)
	{
		throw(std::exception(e)); // we don't want to handle with it here
	}

	return username;
}

void Server::logout_user(string username, string user_id)
{
	
	auto user_it = std::find(this->_users.begin(), this->_users.end(), username);

	if (user_it != this->_users.end()) // username found
	{
		std::unique_lock<mutex> users_locker(this->_users_mu);

		this->_users.erase(user_it);

		users_locker.unlock();

		cout << "REMOVED " << user_id << ", " << username << " from clients list" << std::endl;
	}
	//if he wasn't found than there was an error in the server or something
}

void Server::log_message()
{
	ofstream log_file;

	while (true)
	{
		std::unique_lock<mutex> q_locker(this->_messages_mu);

		if (this->_messages.empty())
		{
			//wating for message
			this->_messages_cond.wait(q_locker, [this]() { return !this->_messages.empty(); });
		}

		Messages message = (this->_messages).back();
		this->_messages.pop();

		q_locker.unlock(); // no need to lock q anymore

		string filename = Messages::get_filename(message.get_sender(), message.get_reciver());

		log_file.open(filename, std::ios::app);

		if (log_file.fail()) // should never happen
		{
			throw(string("Error opening log file!"));
		}

		log_file << "&MAGSH_MESSAGE&&Author&" << message.get_sender() << "&DATA&" << message.get_data();

		log_file.close();
	}

}

string Server::all_users()
{
	string all_users;

	std::unique_lock<mutex> users_locker(this->_users_mu);
	for (auto user : this->_users)
	{
		all_users += user + '&';
	}
	users_locker.unlock();

	all_users.pop_back(); //remove the last &

	//cout << all_users << std::endl;

	return all_users;
}

