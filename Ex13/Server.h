#pragma once

#include <WinSock2.h>
#include <Windows.h>

#include <string>
#include <iostream>
#include <exception>
#include <thread>
#include <queue>
#include <vector>
#include <mutex>
#include <fstream>
#include <algorithm>

#include "Helper.h"
#include "Messages.h"


using std::cout;

using std::ofstream;
using std::ifstream;
using std::mutex;
using std::condition_variable;
using std::string;
using std::vector;
using std::queue;

class Server
{
public:
	
	Server();
	~Server();
	
	void serve(int port);
	void accecpt_clients();
	void log_message();

private:

	SOCKET _serverSocket;
	vector<string> _users;
	queue<Messages> _messages;

	mutex _messages_mu;
	mutex _users_mu;
	condition_variable _messages_cond;

	SOCKET acceptClient();
	void clientHandler(SOCKET clientSocket);
	string login_user(SOCKET clientSocket);
	void logout_user(string username, string user_id);
	string all_users();
	
};

