#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>

#define PORT_NUM 8826

void delete_txt_files();

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		delete_txt_files();

		cout << "Starting..." << std::endl;

		std::thread t(&Server::log_message, &myServer);
		t.detach();

		myServer.serve(PORT_NUM); // connect to a given port number

		myServer.accecpt_clients();
	}
	catch (std::exception& e)
	{
		std::cout << "Error: " << e.what() << std::endl;
	}

	system("PAUSE");
	return 0;
}

void delete_txt_files()
{
	std::string command = "del /Q ";
	std::string path = "*.txt";
	system(command.append(path).c_str());
}